# This projects aims to recreate ScriptHookV in an single easily manually mapped DLL. The file natives.h is generated using this project: [natives.h generator](https://bitbucket.org/gir489/natives.h-generator)  #

##Credits: ##
##gir489: Project lead/Lead developer ##
##s0biet: Original project designer and developer. ##
##[NTAuthority/citizenMP](http://tohjo.eu/citidev/citizenmp): About 70% of the code was RIP in pepperoni'd from them.##
##[Alexander Blade](http://www.dev-c.com/): natives.h ##